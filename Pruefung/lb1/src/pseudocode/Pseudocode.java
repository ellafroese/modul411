package pseudocode;

public class Pseudocode {

    public int rechnen(int n){

        if(n == 1){
            return 1;
        } else if(n == 0){
            return 0;
        } else {
            int e = rechnen(n / 2);
            e = e + (n % 2);
            return e;
        }

    }
}
