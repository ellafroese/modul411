package pseudocode;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Pseudocode p = new Pseudocode();

        Scanner scan = new Scanner(System.in);
        int eingabe = scan.nextInt();

        System.out.println(p.rechnen(eingabe));
    }
}
