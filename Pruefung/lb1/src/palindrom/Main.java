package palindrom;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Palindrom p = new Palindrom();

        Scanner scan = new Scanner(System.in);
        System.out.println("Geben Sie ein Palindrom ein");

        // Eingabe von UserIn
        String palindrom = scan.next();
        // String in char Array speichern
        char[] charArr = palindrom.toCharArray();
        // Für Kontrolle, ob richtig
       // System.out.println(charArr.length);

        // Methode aufrufen Iterativ
        System.out.println(p.erkennenIterativ(charArr));

        // REKURSIVE LÖSUNG
        //PalindromRekursiv pr = new PalindromRekursiv();
        //System.out.println(pr.palindromOK(charArr));

    }
}
