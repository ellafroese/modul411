package palindrom;

public class PalindromRekursiv {

    public boolean palindromOK(char[] array){
        int length = array.length;
        int first = 0;
        int last = array.length -1;

        //Wenn char[] leer ist oder nur noch 1 char hat
        if(length < 2){
            return true;
        }else{
            //Schauen ob first und last die gleichen sind
            if (array[first] != array[last]){
                return false;
            }else {
                first++;
                last--;
                // Selbstaufruf
                return palindromOK(array);
            }
        }
    }

}
