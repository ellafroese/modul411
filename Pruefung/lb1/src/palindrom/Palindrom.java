package palindrom;

public class Palindrom {

    public static boolean erkennenIterativ(char[] array){


        int last = array.length -1;
        int first = 0;


        while (last > first){
            if(array[first] != array[last]){
                return false;
            }
            first++;
            last--;

        }
        return true;
    }
}
