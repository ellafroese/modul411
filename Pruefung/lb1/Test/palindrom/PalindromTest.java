package palindrom;

import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromTest {

    @Test
    public void erkennenIterativKorrekt() {
        assertTrue(Palindrom.erkennenIterativ(new char[]{'a', 'b', 'b', 'a'})== true);
    }


    @Test
    public void erkennenIterativFalsch() {
        assertTrue(Palindrom.erkennenIterativ(new char[]{'h', 'b', 'i', 'a'})== false);
    }

    @Test
    public void erkennenIterativKorrekt2() {
        assertTrue(Palindrom.erkennenIterativ(new char[]{'a', 'b', 'g', 'g', 'b', 'a'})== true);
    }
}