package comparableAufgabe2;

public class Buch implements Comparable<Buch>{
    private String titel;
    private String autor;
    private int anzSeiten;
    private int erscheinungsjahr;

    // Constructor
    public Buch(String titel, String autor, int anzSeiten, int erscheinungsjahr) {
        this.titel = titel;
        this.autor = autor;
        this.anzSeiten = anzSeiten;
        this.erscheinungsjahr = erscheinungsjahr;
    }




    @Override
    public int compareTo(Buch o) {
        if(this.autor.compareTo(o.autor) > 0){
            return 1;
        } else if (this.autor.compareTo(o.autor) > 0){
            return -1;
        } else {
            if (this.erscheinungsjahr < o.erscheinungsjahr){
                return 1;
            } else if(this.erscheinungsjahr > o.erscheinungsjahr){
                return -1;
            } else {
                return 0;
            }
        }

    }

    @Override
    public String toString() {
        return "Autor: " + this.getAutor() + ", Erscheinungsjahr: " + this.getErscheinungsjahr();
    }

    // Getter and Setter Methods
    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getAnzSeiten() {
        return anzSeiten;
    }

    public void setAnzSeiten(int anzSeiten) {
        this.anzSeiten = anzSeiten;
    }

    public int getErscheinungsjahr() {
        return erscheinungsjahr;
    }

    public void setErscheinungsjahr(int erscheinungsjahr) {
        this.erscheinungsjahr = erscheinungsjahr;
    }

}
