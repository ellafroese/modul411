package greedylb2;

import java.util.Arrays;

public class Rechner {

    public static void main(String[] args) {
        Rechner r = new Rechner();


        double[] liste = new double[]{1.80, 1.50, 3.50, 3.00, 6.50, 4.50, 2.00};
        int limit = 20;
        System.out.println(r.rechnen(liste, limit));
    }

    public double rechnen(double[] liste, int limit){
        double[] newListe = new double[liste.length -1];

        Arrays.sort(liste);

        double res = 0;
        int y = 0;

        // Greedy-Algorithmus
        for (int i = liste.length - 1; i >= 0; i--) {
            if (res + liste[i] <= limit) {
                res = res + liste[i];
                newListe[y] = liste[i];
                y++;
            }
        }



        return res;
    }



}
