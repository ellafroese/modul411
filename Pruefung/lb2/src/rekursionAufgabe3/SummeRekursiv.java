package rekursionAufgabe3;

public class SummeRekursiv {

    public int rechnenRek(int a, int b){

        if(b == 0){
            return rechnenRek(a, b) + b;
            // a < b
        } else if (a > b){
            a = a + b;
            b--;
            return a;
        } else {
            a = a + b;
            b--;
            return a;
        }

    }



}
