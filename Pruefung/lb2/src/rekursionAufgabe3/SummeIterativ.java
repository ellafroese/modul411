package rekursionAufgabe3;

public class SummeIterativ {

    public int rechnen(int a, int b){
        int res;
        int big;
        int small ;

        if(a > b){
            res = a;
            big = a;
            small = b;
        } else {
            res = b;
            big = b;
            small = a;
        }


        for(int i = small; i > 0; i--){
            res = res + 1;
        }


        return res;
    }


}
