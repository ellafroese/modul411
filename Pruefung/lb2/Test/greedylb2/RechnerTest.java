package greedylb2;

import static org.junit.Assert.*;

public class RechnerTest {

    @org.junit.Test
    public void rechnen() {
        Rechner r = new Rechner();
        assertTrue(r.rechnen(new double[]{1.80, 1.50, 3.50, 3.00, 6.50, 4.50, 2.00}, 20) == 19.5);
    }
}