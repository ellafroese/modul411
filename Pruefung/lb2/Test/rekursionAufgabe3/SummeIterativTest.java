package rekursionAufgabe3;

import org.junit.Test;

import static org.junit.Assert.*;

public class SummeIterativTest {




    @Test
    public void rechnen5and2() {
        SummeIterativ si = new SummeIterativ();

        assertTrue(si.rechnen(5, 2)== 7);
    }

    @Test
    public void rechnen7and5() {
        SummeIterativ si = new SummeIterativ();

        assertTrue(si.rechnen(7, 5)== 12);
    }

    @Test
    public void rechnen3and3() {
        SummeIterativ si = new SummeIterativ();

        assertTrue(si.rechnen(3, 3)== 6);
    }

    @Test
    public void rechnen4and12() {
        SummeIterativ si = new SummeIterativ();

        assertTrue(si.rechnen(4, 12)== 16);
    }


}