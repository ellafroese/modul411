package rekursionAufgabe3;

import org.junit.Test;

import static org.junit.Assert.*;

public class SummeRekursivTest {

    @Test
    public void rechnenRekTest1() {
        SummeRekursiv sr = new SummeRekursiv();

        assertTrue(sr.rechnenRek(7, 3) == 10);
    }


    @Test
    public void rechnenRekTest3() {
        SummeRekursiv sr = new SummeRekursiv();

        assertTrue(sr.rechnenRek(15, -3) == 12);
    }


}