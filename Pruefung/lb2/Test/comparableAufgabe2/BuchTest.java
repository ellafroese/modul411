package comparableAufgabe2;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BuchTest {

    @Test
    public void testing1() {
        Buch b1 = new Buch("Titel 1", "Autor A", 356, 2020);
        Buch b2 = new Buch("Titel 2", "Autor A", 434, 2021);
        Buch b3 = new Buch("Titel 3", "Autor B", 754, 2005);
        Buch b4 = new Buch("Titel 4", "Autor C", 386, 2003);
        Buch b5 = new Buch("Titel 5", "Autor C", 125, 1956);
        Buch b6 = new Buch("Titel 6", "Autor D", 136, 1975);
        Buch b7 = new Buch("Titel 7", "Autor D", 212, 2007);


        Buch[] buch = new Buch[]{b1, b2, b3, b4, b5, b6, b7};
        Arrays.sort(buch);
        assertTrue(Arrays.toString(buch).equals(new String("[Autor: Autor A, Erscheinungsjahr: 2021, Autor: Autor A, Erscheinungsjahr: 2020, Autor: Autor B, Erscheinungsjahr: 2005, Autor: Autor C, Erscheinungsjahr: 2003, Autor: Autor C, Erscheinungsjahr: 1956, Autor: Autor D, Erscheinungsjahr: 2007, Autor: Autor D, Erscheinungsjahr: 1975]")));

    }
}

