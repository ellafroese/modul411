package aufgabe1;

import org.junit.Test;

import static org.junit.Assert.*;

public class TeilnehmerlisteTest {


    // Die liste ist leer
    @org.junit.Test
    public void isEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();

        assertTrue(tl.isEmpty(tl) == true);

    }


    // Die liste ist nicht leer
    @org.junit.Test
    public void isNotEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();
        Teilnehmer t2 = new Teilnehmer("Muster", "Hans");
        //Teilnehmer hinzufügen
        tl.add(t2);

        assertTrue(tl.isEmpty(tl) == false);

    }


    // Es hat Teilnehmer in der liste --> Index vom 1.
    @Test
    public void getIndexOfFirst() {
        Teilnehmerliste tl = new Teilnehmerliste();

        Teilnehmer t1 = new Teilnehmer("Muster", "Hans");
        //Teilnehmer hinzufügen
        tl.add(t1);

        assertTrue(tl.getIndexOf(t1) == 0);
    }


    // Es hat keine Teilnehmer in der Liste
    @Test
    public void getIndexOfEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();

        Teilnehmer t1 = new Teilnehmer("Muster", "Hans");
        //Teilnehmer hinzufügen

        assertTrue(tl.getIndexOf(t1) == -1);
    }



    // Liste ist leer
    @Test
    public void removeLastEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();

        assertTrue(tl.removeLast(tl) == false);

    }


    // Liste ist leer
    @Test
    public void removeLast() {
        Teilnehmerliste tl = new Teilnehmerliste();
        Teilnehmer t2 = new Teilnehmer("Muster2", "Hans");
        Teilnehmer t3 = new Teilnehmer("Muster3", "Hans");


        assertTrue(tl.removeLast(tl) == true);

    }

    @Test
    public void reverseEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();

        assertEquals(tl.reverse(tl).equals("Leere liste, da nichts drinnen stand"));
    }


    @Test
    public void reverseEmpty() {
        Teilnehmerliste tl = new Teilnehmerliste();
        Teilnehmer t2 = new Teilnehmer("Muster2", "Hans");
        Teilnehmer t3 = new Teilnehmer("Muster3", "Hans");

        assertEquals(tl.reverse(tl).equals("umgekehrte Liste"));
    }

}