package aufgabe2;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) {

        Collection c = new Collection();

        PriorityQueue<Integer> pq = new PriorityQueue<>();

        int i = 1;

        while (i < 10){
            pq.add(i);
            if(1. herausgelöscht werden soll){
                pq.poll();
            }
            i++;
        }





        pq.add(1);      // Hans kommt
        pq.add(2);      // Heidi kommt
        pq.add(3);      // Petra kommt
        // Wer ist erster? 1
        System.out.println(pq.peek());
        pq.poll();      // Hans wird bedient
        // Wer ist erster? 2
        System.out.println(pq.peek());
        pq.poll();      // Heidi wird bedient
        // Wer ist erster? 3
        System.out.println(pq.peek());
        // Es kommt niemand --> nächste nr wird aufgerufen
        pq.poll();      // Petra geht frühzeitig
        // Wer ist erster? null (niemand da)
        System.out.println(pq.peek());


        pq.add(4);      // Anna kommt
        // Wer ist erster? 4
        System.out.println(pq.peek());
        pq.add(5);      // Paul kommt
        pq.poll();      // Anna wird bedient
        // Wer ist erster? 5
        System.out.println(pq.peek());
        // Wer ist erster? null (niemand da)
        // Es kommt niemand --> nächste nr wird aufgerufen
        pq.poll();      // Paul geht frühzeitig
        // Wer ist erster? null
        System.out.println(pq.peek());

        pq.add(6);      //Tina kommt



        System.out.println(pq.peek());


    }


}
