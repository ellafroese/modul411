package aufgabe2;

public class Kunde {
    private int ticketNr;
    private String name;


    public Kunde(int ticketNr, String name) {
        this.ticketNr = ticketNr;
        this.name = name;
    }

    public int getTicketNr() {
        return ticketNr;
    }

    public void setTicketNr(int ticketNr) {
        this.ticketNr = ticketNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
