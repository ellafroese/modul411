package mitZahlen;

import java.util.Arrays;

public class Calculator {

    public static void generate(){
        int[] zahlenArray = new int[]{5, 7, 2, 12, 6, 3, 1};

        // Sortieren des Arrays
        Arrays.sort(zahlenArray);

        calculate(zahlenArray, 35);

        }

    public static int[] calculate(int[] array, int limit){
        int res = 0;
        int y = 0;
        int[] arrayNeu = new int[array.length];

        for(int i = array.length -1; i >= 0; i--){

            if(res + array[i] <= limit){
                res = res + array[i];
                arrayNeu[y] = array[i];
                y++;
            }
        }
        System.out.println(res);
        System.out.println(Arrays.toString(arrayNeu));
        return arrayNeu;
    }
}
