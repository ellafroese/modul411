package gepaekuebung;

public class Gegenstand implements Comparable<Gegenstand> {

    private String name;
    private double gewicht;

    // Constructor
    public Gegenstand(String name, double gewicht) {
        this.name = name;
        this.gewicht = gewicht;
    }


    // Getter and Setter Methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    @Override
    public int compareTo(Gegenstand g){

        if(this.gewicht > g.gewicht){
            // this.gewicht ist grösser
            return 1;

        } else if(this.gewicht < g.gewicht){
            return -1;

        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Gegenstand{" +
                "name='" + name + '\'' +
                ", gewicht=" + gewicht +
                '}';
    }
}
