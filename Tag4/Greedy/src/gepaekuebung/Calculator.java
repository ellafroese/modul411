package gepaekuebung;

import java.util.Arrays;

public class Calculator {

    private Gegenstand[] gegenstaende = new Gegenstand[6];

    public void generate(){
        gegenstaende[0] = new Gegenstand("A", 4.5);
        gegenstaende[1] = new Gegenstand("B", 1.0);
        gegenstaende[2] = new Gegenstand("C", 5.5);
        gegenstaende[3] = new Gegenstand("D", 7.0);
        gegenstaende[4] = new Gegenstand("E", 3.5);
        gegenstaende[5] = new Gegenstand("F", 6.0);

        calculate(gegenstaende, 20);
    }

    public static Gegenstand[] calculate(Gegenstand[] gegenstaende, int limit){
        // Sortierung des Arrays
        Arrays.sort(gegenstaende);

        Gegenstand[] gegenstaendeNeu = new Gegenstand[6];

        // Kontrolle ob richtig sortiert wurde
        // System.out.println(Arrays.toString(gegenstaende));

        double res = 0;
        int y = 0;

        // Greedy-Algorithmus
        for (int i = gegenstaende.length - 1; i >= 0; i--){
            if(res + gegenstaende[i].getGewicht() <= limit){
                res = res + gegenstaende[i].getGewicht();
                gegenstaendeNeu[y] = gegenstaende[i];
                y++;
            }

        }
        System.out.println("Die folgenden Gegenstände können eingepackt werden: ");
        System.out.println(Arrays.toString(gegenstaendeNeu));
        System.out.println("");
        System.out.println("Das Endgewicht ist " + res);

        return gegenstaendeNeu;
    }
}
