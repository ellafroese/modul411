package mitZahlen;

import org.junit.Test;

public class CalculatorTest {

    @Test
    public void calculateArray1() {
        int[] array1 = new int[]{1,6,4,3};

        Calculator.calculate(array1, 12).equals("[6, 4, 1, 0]");
    }

    @Test
    public void calculateArray2() {
        int[] array2 = new int[]{5, 2, 6, 8, 12, 3};

        Calculator.calculate(array2, 35).equals("[12, 8, 6, 5, 3, 0]");
    }
}