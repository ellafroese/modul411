package gepaekuebung;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CalculatorTest {


    @org.junit.Test
    public void calculateNewAray() {
        Gegenstand[] gegenstaende = new Gegenstand[6];

        gegenstaende[0] = new Gegenstand("A", 4.5);
        gegenstaende[1] = new Gegenstand("B", 1.0);
        gegenstaende[2] = new Gegenstand("C", 5.5);
        gegenstaende[3] = new Gegenstand("D", 7.0);
        gegenstaende[4] = new Gegenstand("E", 3.5);
        gegenstaende[5] = new Gegenstand("F", 6.0);

        assertTrue(Arrays.toString(Calculator.calculate(gegenstaende, 20)).equals(new String("[Gegenstand{name='D', gewicht=7.0}, Gegenstand{name='F', gewicht=6.0}, Gegenstand{name='C', gewicht=5.5}, Gegenstand{name='B', gewicht=1.0}, null, null]")));
    }


}