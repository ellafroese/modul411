package einstieg;

public class Person implements Comparable<Person>{

        private String name;
        private String vorname;

        //Constructor
        public Person(String name, String vorname) {
            this.name = name;
            this.vorname = vorname;
        }

        //Getter und Setter Methode
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getVorname() {
            return vorname;
        }

        public void setVorname(String vorname) {
            this.vorname = vorname;
        }


        // Ausgabe von Daten der Person
        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", vorname='" + vorname + '\'' +
                    '}';
        }

        @Override
        public int compareTo(Person p)
        {
            // links --> wird zu negariver Zahl
            if (this.name.compareTo(p.getName()) <= 0 && this.vorname.compareTo(p.getVorname()) < 0){
                return -1;
            }else if(this.name.compareTo(p.getName()) >= 0 && this.vorname.compareTo(p.getVorname()) > 0) {
                return 1;
            }else {
                return 0;
            }

        }
}


