package minmax;

public class MinMaxRek {


    static int minRechnen(int[] array1){

        //erstes element ausserhalb speichern
        int x = array1[0];
        //Zweites array erstellen, welches um 1 kleiner ist als das erste
        int[] array2 = new int[array1.length -1];

        //die elemente von array1 in array2 speichern (array[0] ist ausserhalb gespeichert)
        for(int i = 1; i < array1.length; i ++){
            array2[i-1] = array1[i];
        }

        //wenn array2 mehr als 1 wert hat
        if (array2.length > 1) {
            //es gibt noch ein Array, von welchem das minimum gerechnet werden muss
            return Math.min(x, minRechnen(array2));

        } else {
            if(x < array2[0]){
                return x;
            } else {
                return array2[0];
            }
        }




    }
}
