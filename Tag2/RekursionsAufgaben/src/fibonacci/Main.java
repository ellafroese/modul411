package fibonacci;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        int eingabe = scan.nextInt();

        Fibonacci f = new Fibonacci();
        System.out.println(f.berechnen(eingabe));
    }
}
