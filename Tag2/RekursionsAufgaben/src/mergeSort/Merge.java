package mergeSort;

public class Merge {

    static int[] sort(int[]array1){

        // solange wie array mehr als 1 position hat
        if (array1.length <= 1){
            return array1;

        }

        int arraylength = array1.length;
        int halfOfArray = arraylength / 2;

        int otherHalf = array1.length - halfOfArray;

        int[] array2 = new int[halfOfArray];
        int[] array3 = new int[otherHalf];

        //for first half
        for(int i = 0; i < halfOfArray ; i++){
            array2[i] = array1[i];
        }

        //For other half
        for(int i = 0; i < otherHalf ; i++){
            array3[i] = array1[i + halfOfArray];
        }

        int[] wertArray2 = sort(array2);
        int[] wertArray3 = sort(array3);


        //Von unten nach oben
        int[] finalesArray = new int[array1.length];
        int i = 0;

        int pointer1 = 0;
        int pointer2 = 0;

        while (pointer1 < array2.length || pointer2 < array3.length){

            if(wertArray2[pointer1] < wertArray3[pointer2]){
                finalesArray[i] = wertArray2[pointer1];
                i++;
                pointer1 ++;

                if(pointer1 >= wertArray2.length){
                    finalesArray[i] = wertArray3[pointer2];
                    i++;
                    pointer2 ++;
                }

            }else {
                finalesArray[i] = wertArray3[pointer2];
                i++;
                pointer2 ++;

                if(pointer2 >= wertArray3.length){
                    finalesArray[i] = wertArray2[pointer1];
                    i++;
                    pointer1 ++;
                }
            }

        }

        return finalesArray;
    }
}
