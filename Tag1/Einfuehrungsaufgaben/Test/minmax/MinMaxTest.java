package minmax;

import org.junit.Test;

import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void maxRechnen(){
        assertTrue(MinMax.maxRechnen(new int[]{1,3,5,6}) == 6);
    }

}