package fakultaet;

import org.junit.Test;

import static org.junit.Assert.*;

public class FakultaetTest {

    @Test
    public void rechnen() {
        assertTrue(Fakultaet.rechnen(3) == 6);
    }
}