package sort;

import java.util.Arrays;

public class BubbleSort {

    //Testarray
    static int[] arrayToSort = new int[] {3,2,12,7,345,26,1};

    //Methode for sort
    public static void sort() {
        //Temp variable
        int temp;

        //For schleife --> so oft, wie array lang ist, damit jede position mit allem verglichen wird
         for (int j = 0; j < arrayToSort.length; j++){
            for (int i = 0; i < arrayToSort.length - 1; i++) {
                if (arrayToSort[i] > arrayToSort[i + 1]) {
                    temp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i + 1];
                    arrayToSort[i + 1] = temp;
                }
            }
        }

        System.out.println("Fertig sortiert");
        System.out.println(Arrays.toString(arrayToSort));

    }

}
