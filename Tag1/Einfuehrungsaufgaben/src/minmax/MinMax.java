package minmax;

public class MinMax {


    //Maximum finden
    public static int maxRechnen(int[] array1) {
        //kleinste mögliche Zahl
        int x = 0;
        for (int i = 0; i < array1.length; i++) {
            int n = array1[i];

            if(n > x){
                x = n;
            }
        }
        System.out.print(x + " ist die grösste Zahl");
        System.out.println(" ");

        return x;
    }


    //Minimum finden
    public static int minRechnen(int[] array1) {
        //grösste mögliche Zahl
        int x =  2147483647;
        for (int i = 0; i < array1.length; i++) {
            int n = array1[i];

            if(n < x){
                x = n;
            }
        }
        System.out.print(x + " ist die kleinste Zahl");

        return x;
    }
}

