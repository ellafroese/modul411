package minmax;

public class Main {
    public static void main(String[] args){

        int[] array1 = new int [] {4, 7, 14, 45, 623, 32, 732, 5};

        MinMax mm = new MinMax();
        mm.maxRechnen(array1);
        mm.minRechnen(array1);
    }
}
