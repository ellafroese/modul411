package stern;

public class Stern {

    //int x = Anzahl Linien
    int x = 8;
    int blank = 4;
    int blankPlusOne = 5;

    public void sterne(){


        //Adding lines
        for(int i = 1; i <= blankPlusOne; i++) {
            //Adding Blanks before *
            for (int s = blank; s > 0; s--) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
            blank = blank - 1;

            blank = 4;
            //second * in line
            //--> 0, 1, 2, 3, 4
            for(int s = 0; s > blank; s++){
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }



        blank = 4;
        //Subtract lines
        for(int i = 1; i <= blank; i++) {
            //Adding Blanks before *
            // --> 0, 1, 2, 3
            // i=  1, 2, 3, 4
            for (int s = i-1; s >= 0; s--) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }




        System.out.println();
        System.out.println();

        //Adding Stars in Line
        //Adding line
        for(int i = 1; i < x; i++){

            //Adding stars
            for(int s = 0; s < i; s++){
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }


        x = x-1;
        //Subtract Stars from line
        //Subtract stars
        for(int i = 1; i < x; i++){
            for(int k = x; k > i; k--){
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }

    }

}
