package summe;

import java.util.Scanner;

public class Summe {

    public void rechnen() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Geben Sie eine Zahl ein, bitte");

        int eingabe = scan.nextInt();

        int loesung = 0;
        for(int i = eingabe; i > 0; i--){
            loesung = loesung + i;
        }

        System.out.println("Die Summe ist " + loesung);
    }
}
