package fakultaet;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Fakultaet fakultaet = new Fakultaet();

        Scanner scan = new Scanner(System.in);
        int eingabe = scan.nextInt();

        fakultaet.rechnen(eingabe);
    }
}
