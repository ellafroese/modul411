package fakultaet;

import java.util.Scanner;

public class Fakultaet {


    public static int rechnen(int eingabe){


        int loesung = 1;

        for(int i = eingabe; i > 0; i--){
            loesung = loesung * i;
        }

        System.out.println("Fakultät von " + eingabe + " ist " + loesung);

        return loesung;
    }
}
