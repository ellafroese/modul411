package lineareSuche;

import org.junit.Test;

import static org.junit.Assert.*;

public class LineareSucheTest {

    @Test
    public void suche1() {
        assertTrue(LineareSuche.suche(19) == 5);
    }

    @Test
    public void sucheNichtErfolgreich(){
        assertTrue(LineareSuche.suche(12) == 3);
    }
}