public class GgtIterativ {

    public int ggtIter(int a, int b){

        while (a != b){
            if(a > b){
                a = a - b;
            } else {
                b = b - a;
            }
        }

        return a;

    }
}
