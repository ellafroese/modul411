public class Ggt {

    public int ggt(int a, int b){

        //Abbruchbedingung
        if(a == b){
            return a;
        } else {

            if(a < b){
                return ggt(a, b - a);

            } else {
                return ggt(a - b, b);
            }
        }
    }
}
