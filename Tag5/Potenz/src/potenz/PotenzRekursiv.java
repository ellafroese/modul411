package potenz;

public class PotenzRekursiv {

    public int rechnen(int a, int pot) {
        int temp = a;

        if (pot == 1){
            return a;
        } else {

           return rechnen(a, pot -1) * a;
        }

    }
}
