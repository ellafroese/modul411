package potenz;

import static org.junit.Assert.*;

public class PotenzTest {

    @org.junit.Test
    public void rechnen5and3() {
        Potenz p = new Potenz();

        assertTrue(p.rechnen(5, 3) == 125);
    }

    @org.junit.Test
    public void rechnen5and4() {
        Potenz p = new Potenz();

        assertTrue(p.rechnen(5, 4) == 625);
    }

    @org.junit.Test
    public void rechnen7and6() {
        Potenz p = new Potenz();

        assertTrue(p.rechnen(7, 6) == 117649);
    }

    @org.junit.Test
    public void rechnen3and12() {
        Potenz p = new Potenz();

        assertTrue(p.rechnen(3, 12) == 531441);
    }
}