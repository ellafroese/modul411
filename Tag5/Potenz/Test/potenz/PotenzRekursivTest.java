package potenz;

import org.junit.Test;

import static org.junit.Assert.*;

public class PotenzRekursivTest {

    @Test
    public void rechnen5and3() {
        PotenzRekursiv pr = new PotenzRekursiv();
        assertTrue(pr.rechnen(5, 3) == 125);
    }

    @Test
    public void rechnen5and4() {
        PotenzRekursiv pr = new PotenzRekursiv();
        assertTrue(pr.rechnen(5, 4) == 625);
    }

    @Test
    public void rechnen7and3() {
        PotenzRekursiv pr = new PotenzRekursiv();
        assertTrue(pr.rechnen(7, 3) == 343);
    }
}