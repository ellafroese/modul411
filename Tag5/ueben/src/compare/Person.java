package compare;

public class Person implements Comparable<Person>{
    private String name;
    private int age;
    private double height;

    // Constructor
    public Person(String name, int age, double height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    @Override
    public String toString() {
        return "" + this.height;
    }

    // compare To method
    @Override
    public int compareTo(Person o) {

        if(this.height < o.getHeight()){
            return -1;
        } else if (this.height > o.getHeight()){
            return 1;
        }else{
            return 0;
        }



    }



    // Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


}
