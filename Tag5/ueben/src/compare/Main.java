package compare;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        Person p1 = new Person("Hans", 54, 1.85);
        Person p2 = new Person("Anna", 43, 1.47);
        Person p3 = new Person("Peter", 23, 1.26);
        Person p4 = new Person("Petra", 54, 1.46);
        Person p5 = new Person("Hans", 17, 1.68);
        Person p6 = new Person("Anna", 32, 1.37);


        Person[] p = new Person[]{p1, p2, p3, p4, p5, p6};
        Arrays.sort(p);
        System.out.println(Arrays.toString(p));
        // System.out.println(p6.compareTo(p2));
    }
}
