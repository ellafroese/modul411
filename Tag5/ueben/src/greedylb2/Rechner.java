package greedylb2;

import java.util.Arrays;

public class Rechner {

    public int[] rechnen(int limit) {

        int[] liste = new int[]{4, 6, 2, 3, 7, 9, 53, 34, 34, 12, 15};

        int[] newListe = new int[liste.length - 1];

        Arrays.sort(liste);

        int res = 0;
        int y = 0;

        // Greedy-Algorithmus
        for (int i = liste.length - 1; i >= 0; i--) {
            if (res + liste[i] <= limit) {
                res = res + liste[i];
                newListe[y] = liste[i];
                y++;
            }
        }

        System.out.println("Die Liste sieht wie folgt aus: " + Arrays.toString(newListe));
        System.out.println("Mit diesen Zahlen kann das Limit von " + limit + " erreicht werden");
        System.out.println("Die Summe der Zahlen ist " + res);


        return newListe;
    }

}
