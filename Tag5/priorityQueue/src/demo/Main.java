package demo;

import java.util.PriorityQueue;

public class Main {

    public static void main(String args[]) {
        // Creating empty priority queue
        PriorityQueue<Integer> pQueue = new PriorityQueue<>();


        //--------------------
        // Einstiegsbeispiel
        //--------------------


        // Adding items to the pQueue using add()
        pQueue.add(10);
        pQueue.add(20);
        pQueue.add(15);
        pQueue.add(6);


        // System.out.println("Printing top element " + pQueue.peek());

        // System.out.println("Printing top element and delete it " +pQueue.poll());

        // System.out.println("Printing Queue " +pQueue);


//-------------------------------------------------------------------------------------------


        //--------------------
        // Einstiegsbeispiel
        // Mit String anstelle Integer
        //--------------------


        PriorityQueue<String> pq1 = new PriorityQueue<>();

        pq1.add("Geeks");
        pq1.add("For");
        pq1.add("Geeks");

        //System.out.println("Initial PriorityQueue " + pq1);


//-------------------------------------------------------------------------------------------


        //--------------------
        // Mit Remove
        // Angeben was gelöscht wird
        // poll löscht die erste Priorität
        //--------------------


        // using the method
        pq1.remove("Geeks");

        //System.out.println("After Remove - " + pq1);

        // System.out.println("Poll Method - " + pq1.poll());

        //System.out.println("Final PriorityQueue - " + pq1);


//-------------------------------------------------------------------------------------------

        //--------------------
        // PriorityQueue sortiert die Liste automatisch
        //--------------------

        // Creating a priority queue
        PriorityQueue<String> pq2 = new PriorityQueue<>();
        pq2.add("Geeks");
        pq2.add("For");
        pq2.add("Geeks");
        //System.out.println("PriorityQueue: " + pq2);

        // Using the peek() method
        String element = pq2.peek();
        //System.out.println("Accessed Element: " + element);


//-------------------------------------------------------------------------------------------


    }


}
