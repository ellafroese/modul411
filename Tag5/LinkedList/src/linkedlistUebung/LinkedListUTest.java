package linkedlistUebung;

import java.util.Arrays;

import static org.junit.Assert.*;

public class LinkedListUTest {

    @org.junit.Test
    public void remove() {
        LinkedListU list = new LinkedListU();
        list.add(4);
        list.add(6);
        list.add(8);
        list.add(1);

        assertTrue(list.remove(6) == true);
    }
}