package linkedlistUebung;

import javafx.beans.binding.When;

public class LinkedListU {

    private EintragLL first;

    public void add(int value) {

        if (first == null) {
            // Liste hat noch keinen Eintrag
            first = new EintragLL(value, null);
        } else {
            // Liste hat bereits einen Eintrag
            EintragLL newEintrag = new EintragLL(value, null);

            EintragLL temp = first;

            while (temp.getNext() != null) {
                temp = temp.getNext();
            }

            temp.setNext(newEintrag);

        }
    }

    public boolean contains(int value) {
        EintragLL temp = first;

        while (temp.getValue() != value) {
            if (temp.getNext() != null) {
                temp = temp.getNext();
            } else {
                return false;
            }
        }
        return true;
    }


    public boolean remove(int value) {
        EintragLL temp = first;

        if (first.getValue() == value) {
            first = first.getNext();
        } else {
            while (temp.getNext().getValue() != value) {
                temp = temp.getNext();
            }
            temp.setNext(temp.getNext().getNext());
            return true;
        }
        return false;
    }


    public boolean removeLast() {
        EintragLL temp = first;

        while (temp.getNext().getNext() != null) {
            while (temp.getNext().getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(temp.getNext().getNext());
            return true;
        }
        return false;

    }


    public String print() {

        EintragLL temp = first;

        String out = "" + temp.getValue();

        while (temp.getNext() != null) {
            temp = temp.getNext();
            out = out + ", " + temp.getValue();
        }
        return out;
    }


    public int size() {
        EintragLL temp = first;
        int count = 0;
        while (temp != null) {
            temp = temp.getNext();
            count++;
        }
        return count;
    }

    public int getIndex(int value) {
        int count = 0;

        EintragLL temp = first;

        while (temp.getNext() != null) {
            if (temp.getValue() == value) {
                return count;
            }
            temp = temp.getNext();
            count++;
        }
        return count;
    }


}
