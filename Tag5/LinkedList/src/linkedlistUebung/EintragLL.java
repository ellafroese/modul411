package linkedlistUebung;

public class EintragLL {
    //Attribute
    private int value;
    private  EintragLL next;



    //----------------------------------------------------------
    // Constructor
    public EintragLL(int value, EintragLL next) {
        this.value = value;
        this.next = next;
    }



    //----------------------------------------------------------
    // Getter and Setter Methods
    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public EintragLL getNext() {
        return next;
    }

    public void setNext(EintragLL next) {
        this.next = next;
    }
}
